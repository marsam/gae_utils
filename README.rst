GAE Utils
=========

Useful utilities for working with `google appengine <https://developers.google.com/appengine/>`_.

Functions
---------

``is_appengine``
    Retuns ``True`` if is currently in appengine servers.

``find_gae_path``
    Retuns the path where the gae sdk is installed. ``None`` if is not found.

``patch_path``
    Add the libraries included with the gae sdk to the PYTHONPATH.

``google_stubs``
    Setup google appeengine stubs, especially useful for testing::

        google_stubs(use_sqlite=True, clear_datastore=True)
