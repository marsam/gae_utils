# -*- coding: utf-8 -*-

from os import path, getenv
from distutils.spawn import find_executable


def is_appengine():
    return getenv('SERVER_SOFTWARE', '').startswith('Google App Engine')


def find_gae_path():
    appserver = find_executable('dev_appserver.py')
    return path.dirname(appserver or '') or getenv('GAE_SDK_ROOT')


def patch_path():
    gae_path = find_gae_path()
    if not gae_path:
        raise Warning('Google appengine sdk not found')
    import sys
    sys.path.insert(0, gae_path)
    from dev_appserver import EXTRA_PATHS
    sys.path = EXTRA_PATHS + sys.path


def google_stubs(**kwargs):
    """
    Setup google stubs.
    """
    from google.appengine.tools import dev_appserver
    from google.appengine.tools import dev_appserver_main
    config = dev_appserver_main.DEFAULT_ARGS
    config.update(kwargs)
    dev_appserver.SetupStubs('local', **config)
