#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup

readme = open('README.rst').read()

setup(
    name='gae_utils',
    version='0.2.2',
    author='mario rodas',
    author_email='rodasmario2@gmail.com',
    url='https://bitbucket.org/marsam/gae_utils',
    packages=['gae_utils'],
    platforms='any',
    license='MIT',
    description='Useful utilities for working with gae',
    long_description=readme,
    classifiers=[
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.1',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'License :: OSI Approved :: MIT License',
    ],
)
